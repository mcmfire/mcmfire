import React from "react";
import "./home.scss";

const Home = () => {
    return (
        <section>
            <div className="home-page">
                <img src="../../assets/icons/avatar.svg" alt="banner-avatar"/>
            </div>
        </section>
    );
};

export default Home;